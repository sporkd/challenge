import { createWebHistory, createRouter } from "vue-router";

const routes =  [
  {
    path: "/",
    alias: "/transactions",
    name: "transactions",
    component: () => import("./components/TransactionsList")
  },
  {
    path: "/transactions/:id",
    name: "transaction-details",
    component: () => import("./components/Transaction")
  },
  {
    path: "/add",
    name: "add",
    component: () => import("./components/AddTransaction")
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
